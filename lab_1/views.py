from django.shortcuts import render

def index(request):
    response = {
        'name' : 'Ariq Munif Kusuma',
        'school' : 'SMAN 2 Cirebon',
        'npm' : '1706039692',
    }
    return render(request, 'lab1.html', response)
